import { Component, OnInit } from '@angular/core';
import { HeaderService } from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  public header: string='';
 
  constructor(private service: HeaderService) { }
 
  ngOnInit() {
    this.header = this.service.getHeader();
  }

}
