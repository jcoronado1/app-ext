import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { HeaderService } from './header.service';



@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [HeaderComponent],
  providers: [HeaderService]
})
export class HeaderModule {
  public static forRoot(): ModuleWithProviders <any> {
    return {
      ngModule: HeaderModule,
      providers: [
        HeaderService
      ]
    };
  }
}